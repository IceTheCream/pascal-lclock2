const ciMonthCount          = 12;
      ciWeekDayCount        = 7;
      ciMaxSecond           = 59;
      ciMaxMinute           = 59;
      ciMaxHour             = 23;
      ciMicroWidth          = 8;
      ciMicroHeight         = 8;
      ciNoTimeDateLeft      = 9;
      ciDefaultTimeColor    = clNavy;
      ciDefaultDateColor    = clGreen;

      {
      ciNormalTimeWidth     = 80;
      ciNormalDateLeft      = 112;
      }
      csIniFileName         = 'lclock2.ini';
      csWinIniFileName      = 'windows.ini';

      csEtcFolder           = 'etc'+ccSlashChar;
      csLocaleFolder        = 'locale'+ccSlashChar;
      csConfigSection       = 'config';
      csStickyFlag          = 'stickyflag';
      csStickyMargin        = 'stickymargin';
      csTransparentFlag     = 'transparentflag';
      csTransparentValue    = 'transparentvalue';
      csMinimizeBtnVisible  = 'minimizevisibleflag';
      csCloseBtnVisible     = 'closevisibleflag';
      csVisibleTimeFlag     = 'visibletimeflag';
      csVisibleDateFlag     = 'visibledateflag';
      csTimeFontValue       = 'timefontvalue';
      csDateFontValue       = 'datefontvalue';
      csTimeColorValue      = 'timecolorvalue';
      csDateColorValue      = 'datecolorvalue';
      csThemeNameValue      = 'themenamevalue';

      csDefaultTimeFont     = 'Verdana;1;-15;D;12;0';
      csDefaultDateFont     = 'Verdana;1;-15;D;12;0';

      csProgramName         = 'LClock';
      csVersion             = '0.0.1';

      //***** Темы
      csThemeFolder         = 'themes'+ccSlashChar;
      csDefaultThemeName    = 'default.ini';
      csMicroSection        = 'micro';
      csDefaultMicroFolder  = 'pixmaps'+ccSlashChar+'micro'+ccSlashChar+'rounded'+ccSlashChar;
      csButtonsSection      = 'buttons';
      csDefaultButtonFolder = 'pixmaps'+ccSlashChar+'buttons'+ccSlashChar+'oxygen'+ccSlashChar;

      //***** Локаль
      csMainLocaleFilename  = 'main.ini';
      csFormsLocaleFilename = 'forms.ini';
      csMainPopUpItemSetup  = 'PopUpItem_Config';
      csMainPopUpItemExit   = 'PopUpItem_Exit';

